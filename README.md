# MFH Hub

# Usage
```
[ SAP DEFAULT USER ]
SYSTEM

[ SAP DEFAULT DATABASE ]
HXE

[ SAP DEFAULT PORT ( CONTAINER )]
39017

[ SAP INSTANCE NUMBER ]
90

[ SAP DPSERVER PORT ]
39040

[ SAP TENANT PORT ]
39041
39042 ( temporary )
+ Operation is 'cockpit' use

[ SAP HTTP PORT ]
8090
```

## Sql access 
```bash
$ hdbsql -i {instanceNumber} -d {databaseName} -u {userName} -p {userPassword} -n {ip}:{port} 
```

## Install Helm chart 
```bash
$ helm install {releaseName} -n {namespace} -f values ./
```
